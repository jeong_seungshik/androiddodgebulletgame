package com.example.dodgebulletgame;

import android.content.Context;

public class Tiger extends Character {

    float offset_x = 0, offset_y = 0;

    public Tiger(Context context){
        super(context);
    }

    public void calculateOffset(float to_x, float to_y, float radius, float speed){

        float _speed = (float)Math.sqrt(speed);
        double dist = Math.sqrt(Math.pow((to_x - x), 2) + Math.pow((to_y - y), 2));
        offset_x = (to_x - x) / (float)dist * _speed;
        offset_y = (to_y - y) / (float)dist * _speed;
    }

    float[] getOffset(){
        return new float[]{offset_x, offset_y};
    }
}
