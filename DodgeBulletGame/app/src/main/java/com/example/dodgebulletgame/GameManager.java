package com.example.dodgebulletgame;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.TimerTask;

public class GameManager extends TimerTask {

    private Context context;

    private static final int WAITING = 0;
    private static final int PLAYING = 1;

    private int manager_state = WAITING;

    private Character bunny;
    private ArrayList<Tiger> tigers;

    private static int BUNNY_SIZE = 20;
    private static int TIGER_SIZE = 25;

    private static int MAX_LEVEL = 30;
    private static float MAX_TIGER_SPEED = 1.5f;
    private static int MIN_TIGER_SPAWN_TICK = 5;
    private int level = 5;

    private long tickElapsed = 0;

    private float bunny_speed = 3, tiger_speed = 1;

    private int tiger_spawn_tick = 50;

    private float center_x, center_y, map_radius;

    private RelativeLayout layout;

    private int point = 0;

    private Coord[] coord = {
            new Coord(-1, -1),
            new Coord(0, -1),
            new Coord(1, -1),
            new Coord(-1, 0),
            new Coord(0, 0),
            new Coord(1, 0),
            new Coord(-1, 1),
            new Coord(0, 1),
            new Coord(1, 1),};

    static {
        System.loadLibrary("fpga-text-lcd-jni");
        System.loadLibrary("fpga-buzzer-jni");
        System.loadLibrary("fpga-push-switch-jni");
    }

    GameManager(Context _context, float cx, float cy, float mr) {
        context = _context;

        bunny = new Character(context);
        tigers = new ArrayList<>();

        center_x = cx;
        center_y = cy;
        map_radius = mr;

        layout = ((Activity) context).findViewById(R.id.mainLayout);

        bunny.setImageResource(R.drawable.bunny);

        bunny.setCoord(center_x, center_y);
        bunny.setSize(BUNNY_SIZE, BUNNY_SIZE);

        layout.addView(bunny);

        DeviceOpen();
        ReceiveTextLcdValue("PRESS MIDDLE", "TO START");

    }

    class Coord {
        public float x;
        public float y;

        Coord(float _x, float _y) {
            x = _x;
            y = _y;
        }
    }

    @Override
    public void run() {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int sw_val = ReceivePushSwitchValue();

                if (manager_state == WAITING && sw_val == 4) {

                    removeAllViews();
                    tigers.clear();
                    bunny.setCoord(center_x, center_y);
                    point = 0;
                    level = 5;
                    tickElapsed = 0;
                    layout.addView(bunny);

                    manager_state = PLAYING;
                } else if (manager_state == PLAYING) {
                    if (sw_val >= 0 && checkBunny(coord[sw_val].x, coord[sw_val].y)) {
                        float speed = (float) Math.sqrt(bunny_speed);
                        bunny.move(coord[sw_val].x * speed, coord[sw_val].y * speed);
                    }

                    if (tickElapsed % tiger_spawn_tick == 0) // 0.5sec elapsed, 1tick = 10ms
                    {
                        while (tigers.size() < level) {
                            addTiger();
                        }
                    }

                    if (tickElapsed % 100 == 0) {
                        point++;
                        String str = "point: " + String.format("%d", point);
                        ReceiveTextLcdValue(str, " ");
                    }

                    if (tickElapsed == 300) {
                        if (level < MAX_LEVEL) {
                            level++;
                        }

                        if (tiger_spawn_tick < MIN_TIGER_SPAWN_TICK)
                            tiger_spawn_tick -= 5;

                        tickElapsed = 0;
                    }

                    tickElapsed++;

                    moveTigers();
                    checkCollision();

                }
            }

        });
    }

    private void moveTigers() {
        checkTigers();

        for (Tiger tiger : tigers) {
            float[] coord = tiger.getOffset();
            tiger.move(coord[0], coord[1]);
        }
    }

    private void checkTigers() {
        for (int i = 0; i < tigers.size(); ++i) {
            float[] coord = tigers.get(i).getCoord();
            if (Math.sqrt(Math.pow(coord[0] - center_x, 2) + Math.pow(coord[1] - center_y, 2)) > map_radius) {
                removeTiger(i);
            }
        }
    }

    private boolean checkBunny(float x, float y) {
        float[] coord = bunny.getCoord();
        if (Math.sqrt(Math.pow(coord[0] + x - center_x, 2) + Math.pow(coord[1] + y - center_y, 2)) > map_radius) {
            return false;
        }

        return true;
    }


    private void addTiger() {
        Tiger tiger = new Tiger(context);
        tiger.setSize(TIGER_SIZE, TIGER_SIZE);
        tiger.setImageResource(R.drawable.tiger);

        float[] tc = calculateRandomCoordinate(map_radius);
        tc[0] += center_x;
        tc[1] += center_y;
        tiger.setCoord(tc);

        float[] bunny_coord = bunny.getCoord();
        tiger.calculateOffset(bunny_coord[0], bunny_coord[1], map_radius, tiger_speed);

        layout.addView(tiger);
        tigers.add(tiger);
    }

    private void removeTiger(int index) {
        layout.removeView(tigers.get(index));
        tigers.remove(index);
    }

    private float[] calculateRandomCoordinate(float r) {
        r -= 40;
        double seed = Math.random() * 2 * Math.PI;

        return new float[]{(float) (r * Math.cos(seed)), -(float) (r * Math.sin(seed))};
    }

    private void checkCollision() {
        for (int i = 0; i < tigers.size(); ++i) {
            float[] tc = tigers.get(i).getCoord();
            float[] bc = bunny.getCoord();
            float[] bs = bunny.getSize();
            if ((bc[0] - bs[0] / 2 < tc[0] && tc[0] < bc[0] + bs[0] / 2) && (bc[1] - bs[1] / 2 < tc[1] && tc[1] < bc[1] + bs[1] / 2)) {
                String str = "Your point: " + String.format("%d", point);
                ReceiveTextLcdValue(str, "MIDDLE TO RESET");
                ReceiveBuzzerValue(1);
                SystemClock.sleep(2000);
                ReceiveBuzzerValue(0);

                manager_state = WAITING;
            }
        }
    }

    public void removeAllViews() {
        if (bunny.getParent() != null) {
            layout.removeView(bunny);
        }
        for (Tiger tiger : tigers) {
            if (tiger.getParent() != null) {
                layout.removeView(tiger);
            }
        }
    }


    public native int ReceiveTextLcdValue(String ptr1, String ptr2);

    public native int ReceiveBuzzerValue(int x);

    public native int DeviceOpen();

    public native int DeviceClose();

    public native int ReceivePushSwitchValue();
}
