package com.example.dodgebulletgame;

import android.content.Context;
import android.graphics.Canvas;
import android.view.Gravity;
import android.view.ViewGroup;

public class Character extends androidx.appcompat.widget.AppCompatImageView {

    float x;
    float y;
    ViewGroup.LayoutParams params;


    public Character(Context context){
        super(context);

        x = y = 0;
        params = new ViewGroup.LayoutParams(40, 40);
        setLayoutParams(params);
    }

    public void _setX(float _x){
        x = _x;
        setTranslationX(x - params.width / 2.0f);
    }

    public void _setY(float _y){
        y = _y;
        setTranslationY(y - params.height / 2.0f);
    }

    public void setCoord(float _x, float _y){
        x = _x;
        y = _y;
        setTranslationX(x);
        setTranslationY(y);
    }

    public void setCoord(float[] coord){
        x = coord[0];
        y = coord[1];
        setTranslationX(x);
        setTranslationY(y);
    }

    public void setSize(int width, int height){
        params.width = width;
        params.height = height;

        setLayoutParams(params);
    }

    public float[] getSize(){
        return new float[]{params.width, params.height};
    }

    public void move(float _x, float _y){
        x += _x;
        y += _y;
        setTranslationX(x);
        setTranslationY(y);
    }

    public float[] getCoord(){
        float[] ret = new float[2];
        ret[0] = x;
        ret[1] = y;

        return ret;
    }

}
