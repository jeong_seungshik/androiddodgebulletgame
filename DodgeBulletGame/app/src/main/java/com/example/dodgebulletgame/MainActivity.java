package com.example.dodgebulletgame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import java.util.Timer;


public class MainActivity extends AppCompatActivity{

    // Used to load the 'native-lib' library on application startup.
    int isInitialized = 0;
    float map_r;
    float center_x, center_y;
    GameManager manager;

    static {
        System.loadLibrary("fpga-text-lcd-jni");
        System.loadLibrary("fpga-buzzer-jni");
        System.loadLibrary("fpga-push-switch-jni");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if(isInitialized == 0)
        {
            View map = findViewById(R.id.map);
            center_x = 200;
            center_y = 400/ 2.0f;
            map_r = map.getWidth() / 2.0f + 20f;

            manager = new GameManager(this, center_x, center_y, map_r);
            Timer timer = new Timer();
            timer.schedule(manager, 0, 10);
            isInitialized++;
        }
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */


}
