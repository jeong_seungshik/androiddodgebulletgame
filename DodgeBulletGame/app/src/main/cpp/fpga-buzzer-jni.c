//
// Created by JEONG on 2020-12-15(015).
//

#include <string.h>
#include <jni.h>
#include <android/log.h>
#include <fcntl.h>
#include <unistd.h>

#define BUZZER_DEVICE "/dev/fpga_buzzer"

int fpga_buzzer(int x)
{
    int dev;
    unsigned char data;
    unsigned char retval;

    data = (unsigned char)x;

    dev = open(BUZZER_DEVICE, O_RDWR);

    if(dev < 0)
    {
        __android_log_print(ANDROID_LOG_INFO, "Device Open Error", "Driver = %d", dev);
        return -1;
    } else{
        __android_log_print(ANDROID_LOG_INFO, "Device Open Success", "Driver = %d", x);
        write(dev, &data, 1);
        close(dev);
        return 0;
    }
}


JNIEXPORT jint JNICALL
Java_com_example_dodgebulletgame_GameManager_ReceiveBuzzerValue(JNIEnv *env, jobject thiz,
                                                                 jint val){
    jint result;
    __android_log_print(ANDROID_LOG_INFO, "FpgaBuzzerExample", " valuse = %d", val);
    result = fpga_buzzer(val);

    return result;
}

